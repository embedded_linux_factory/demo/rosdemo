# ROS Scheduler

## Linux Kernel Scheduler for ROS 1

Unlike ROS 2, ROS 1 relies on the Linux kernel scheduler for managing tasks and processes. The Linux scheduler prioritizes tasks based on pre-defined algorithms, ensuring efficient resource allocation and execution.

The Linux kernel scheduler acts as a critical component for managing processes within the ROS 1 environment. It employs various algorithms to allocate CPU resources efficiently and fairly among these processes, ensuring smooth operation of the robot system.

### Time Slicing and Preemption

The scheduler divides CPU time into small intervals, often referred to as "time slices." Each runnable process receives a time slice to execute its instructions.  If a higher-priority process becomes ready to run while a lower-priority process is utilizing the CPU, preemption occurs. The lower-priority process is temporarily paused, and the higher-priority process takes over to ensure responsiveness for critical tasks.

### Prioritization Mechanisms

The Linux kernel scheduler utilizes a well-defined set of techniques to determine process execution order. These techniques include:
- Nice Values: Each process is assigned a "nice" value ranging from -20 (highest priority) to 19 (lowest priority). Lower nice values signify higher priority. System administrators or ROS applications can strategically adjust these values to influence process scheduling. For example, a critical motor control process might be assigned a nice value of -2, ensuring it receives CPU resources before a lower-priority sensor data processing task with a nice value of 10.
- Scheduling Classes: Processes are categorized into distinct scheduling classes, each with its own prioritization scheme. The most common class, the "Completely Fair Scheduler" (CFS), prioritizes fairness by ensuring all runnable processes receive CPU resources proportionally over time. This approach is well-suited for most robot applications where various processes contribute but don't have strict timing requirements. However, for scenarios demanding stricter guarantees, like real-time control systems, alternative classes like "Real-time" classes exist. These classes offer deterministic behavior, ensuring critical tasks are executed within a predefined time frame.

In essence, the Linux kernel scheduler employs a hierarchical approach using nice values and scheduling classes to manage process execution order.

### Linux Scheduling Mechanism

The Linux kernel scheduler implements a hierarchical scheduling approach, with multiple layers managing CPU allocation.

#### Scheduling Domains

The Linux scheduler organizes physical CPU cores into groups called scheduling domains. These domains can encompass cores on the same chip or multiple chips within a system. Processes are assigned to scheduling domains based on hardware proximity. This structure allows for efficient task distribution and reduces migration costs when processes need to move between cores.

#### Multi-Queue Scheduler

The Linux kernel maintains a separate run queue for each logical CPU (vCPU) within the system. Each runnable process waits for its turn to execute on a specific vCPU within its assigned scheduling domain. The scheduler strategically migrates vCPUs between run queues to optimize resource utilization and workload distribution.

#### CPU Pinning

While CPU pinning can seem like a straightforward way to optimize process allocation, it's generally discouraged due to its dependence on dynamic factors.  These factors include host reboots, workload fluctuations, and virtual server modifications.  In such scenarios, pinning can have the opposite effect, hindering performance.


![Linux Scheduling mechanism](https://www.ibm.com/docs/en/linuxonibm/com.ibm.linux.z.ldva/ldva_g_scheduling.png)


### Optimizing Resource Utilization

The Linux kernel scheduler goes beyond basic prioritization. It also considers the overall system load and CPU architecture to optimize task distribution:
- Load Balancing Across Cores:  For systems with multiple CPU cores, the scheduler can distribute processes across them to prevent overloading a single core. This strategy improves overall system performance by leveraging available processing power.
- CPU Affinity:  For processes that heavily rely on inter-core communication (e.g., sensor data processing pipelines), the scheduler can "pin" them to specific CPU cores. This minimizes communication delays and optimizes performance by ensuring processes with frequent communication reside on the same cores.

By employing this combination of mechanisms, the Linux kernel scheduler strives to allocate CPU resources efficiently and fairly for processes running within a ROS 1 environment. This sophisticated prioritization system ensures critical robot tasks receive the necessary resources for timely execution, contributing to the overall responsiveness and performance of your robotic system.


### Scheduler Policy

The Linux kernel uses various scheduling policies to execute multiple tasks simultaneously. The aim is to distribute CPU time fairly between processes by assigning them priorities. Applications can benefit from improved performance when the administrator chooses how processes are executed. The Linux kernel offers several scheduling policies, including the normal policy (SCHED_OTHER) for standard-priority tasks and the real-time policy (SCHED_FIFO and SCHED_RR) for urgent tasks with no time division.

#### Normal planning with SCHED_OTHER

The SCHED_OTHER scheduling policy uses the CFS scheduler to distribute CPU resources fairly between processes. It creates a dynamic priority list based on the static priority of each process. Users can adjust the static priority, but the dynamic priority is determined by factors such as sleep time.

#### Static priority scheduling with SCHED_FIFO

SCHED_FIFO assigns a fixed priority to each thread, improving response time and reducing latency. The scheduler orders threads by priority, executing the highest-priority thread first. If two threads have the same priority, the current one runs to completion before giving way to the other.

#### Process priorities

Process priorities are defined in groups, with some groups dedicated to certain kernel functions. For real-time scheduling policies, priorities range from 1 (lowest) to 99 (highest).

| Priority | thread's types | Description |
|---|---|---|
| 1 | Low-priority kernel threads | This priority is generally reserved for tasks that need to be just above SCHED_OTHER (priority 0)  |
| 2 to 49 | Available for use | This priority range is used for classic application priorities. |
| 50 Execution Control	 | Default value for hardware interrupts | / |
| 51 to 98 | High-priority threads | This range is used for threads running periodically and requiring a fast response time. This range must not be used by processes linked to a processor, otherwise they will be unable to interrupt it. |
| 99 | Watchdogs and Thread migration between processors | This priority is reserved for threads that must run with the highest priority |

## ROS 2 scheduler

The way ROS 2 handles tasks within a robot is fundamentally different from ROS 1. We have already explained how task's execution on ROS 1 works in a single program, based on Linux kernel scheduler. ROS 2 takes a different approach, focusing on individual ROS 2 nodes. Each node has its own built-in "mini traffic controller" called an Executor.


### Executors

Execution management in ROS 2 is handled by Executors. An executor is a dedicated conductor specifically managing the execution of tasks (callbacks) within a single ROS 2 node. These callbacks are triggered by various events, such as receiving a message on a topic or a timer expiring. Unlike the Linux kernel scheduler, which oversees processes across the entire system, Executors provide a more focused and controlled environment for task execution within a node's boundaries. Executor's class ise provided by rclcpp, rclpy and rclc librairies.

This shift towards having Executors manage tasks within each node offers several advantages for robot software development:
- More Control: Developers have more control over how tasks are executed within their ROS 2 nodes. This allows them to prioritize critical tasks and ensure they run smoothly, especially for real-time applications where timing is crucial.
- Improved Responsiveness: By having dedicated Executors for each node, ROS 2 can potentially react faster to events and sensor data. This can be important for robots that need to make quick decisions or take immediate actions.
- Increased Predictability: With Executors managing tasks within a controlled environment, the overall behavior of a ROS 2 system becomes more predictable. This is essential for robots that need to operate reliably and consistently.

You're absolutely correct. The additional functionalities introduced in ROS 2, like Executors, can become a double-edged sword. While they offer benefits like finer-grained control and potentially improved responsiveness, if not managed well, they can indeed lead to performance slowdowns and hinder your application. 

#### rclc, rclpp, rclpy

In the ROS2 execution ecosystem we have:
- rclcpp (ROS Client Components in C++): Provides the C++ API for working with ROS 2. It offers functionalities for node creation, topic communication (publish/subscribe), service calls, timers, and, most importantly, callback execution. The rclcpp::Executor class is responsible for scheduling and running callbacks within your ROS 2 node.
- rclpy (ROS Client Components in Python): The Python counterpart to rclcpp. It enables ROS 2 development in Python, providing similar functionalities for node management, communication, and callback handling. Just like rclcpp, rclpy offers an rclpy.Executor class to manage callback execution.

Outside the ROS2 execution ecosystem:
- rclc (micro-ROS Client Library): A lightweight library designed for resource-constrained environments like micro-robots. It prioritizes real-time execution and deterministic behavior, offering features not found in rclcpp or rclpy.

While rclcpp and rclpy share the same objective of managing ROS 2 communication in their respective languages, they differ in scope:
- rclcpp and rclpy: Cater to the general ROS 2 ecosystem, focusing on node communication and callback execution within a ROS 2 node.
- rclc (micro-ROS Client Library): Stands outside the core ROS 2 framework. It emphasizes real-time execution for resource-constrained environments like micro-robots. rclc offers more fine-grained control over callback scheduling compared to rclcpp and rclpy.

Here's a features breakdown of each libraries : 

| Feature | rclcpp/rclpy | rclc |
|---|---|---|
| Primary Use Case | - General ROS 2 communication	 | - Real-time ROS 2 communication (micro-ROS) |
| Language | - C++, Python | - C |
| Callback Execution Control	 | - Limited (basic scheduling) | - Extensive (user-defined order, triggers, priorities) |
| Real-time Focus | - Lower | - Higher |
| Multi-threading Support | - No | - Under development |
| LET Semantics (Data Synchronization) | - No | - Planned integration |
| Additional Notes | - Focuses on ease of use and compatibility within ROS 2 framework | - More complex API, requires deeper understanding of real-time concepts |


#### How Executors Work ?

Here's an Executor's core functionality:

- Call of spin() function: When you call the spin() function on an Executor instance, it essentially tells the Executor to start managing task execution within your node.
- Querying for incoming events: The Executor continuously queries two layers:
    - rcl (ROS Client Library): This layer provides functions for working with ROS entities like nodes, topics, and services. The Executor checks for any incoming messages or events (e.g., timer expirations) published to topics that your node is subscribed to.
    - Middleware: This layer acts as a bridge between ROS and the underlying communication system (e.g., DDS, RTPS). The Executor inquires about the availability of messages received through the middleware.
- Wait Sets for Efficiency: To efficiently track available messages and timer expirations, the Executor utilizes a concept called a wait set. This wait set acts as a signaling mechanism that informs the Executor about events requiring its attention. It maintains a binary flag (0 or 1) for each queue (topic) that your node is subscribed to. If a message arrives for a particular topic, the corresponding flag in the wait set is flipped to 1, indicating the presence of a message that needs processing.
- Callback Execution: When the Executor detects an event through the wait set (e.g., a message arrival or timer expiration), it retrieves the corresponding data and triggers the relevant callback function. Callback functions are pieces of code you define within your ROS 2 node to handle specific events.
- Looping Until Shutdown: The spin() function keeps the Executor running in a loop, continuously querying for events and executing callbacks until the node is shut down.

![About Executors](https://docs.ros.org/en/rolling/_images/executors_basic_principle.png)


#### Type of Executors

ROS 2 provides three main types of Executors, each catering to different use cases:
- Multi-Threaded Executor: This Executor creates a configurable number of threads, allowing parallel processing of multiple messages or events within a single node. This is ideal for nodes that handle a high volume of incoming data and require responsiveness.
- Static Single-Threaded Executor: This Executor is optimized for nodes with a fixed structure (subscriptions, timers, services, etc.) defined during initialization. It performs the initial scan for these elements only once, minimizing runtime overhead. This Executor utilizes a single thread for processing tasks within the node. It's suitable for nodes that don't require extensive dynamic reconfiguration during operation.
- Single-Threaded Executor (rclcpp only): This is a simplified version of the Static Single-Threaded Executor and is available only in the rclcpp library. It also uses a single thread for processing tasks but incurs a slight overhead compared to the Static Single-Threaded Executor due to more frequent scanning for changes in the node's structure.

The selection of the appropriate Executor type depends on your node's specific requirements. Consider these factors when making your choice:
- Number of threads: If your node deals with a high volume of data and requires parallel processing, a Multi-Threaded Executor is preferable.
- Node structure: For nodes with a static structure defined at initialization, the Static Single-Threaded Executor offers better performance.
- Dynamic reconfiguration: If your node's structure changes frequently during operation, avoid the Static Single-Threaded Executor due to its overhead in rescanning for changes.
- Library preference: If you're using the rclcpp library, you have the additional option of the Single-Threaded Executor, which is a simpler version of the Static Single-Threaded Executor.


#### Callback Groups

ROS 2 introduces Callback Groups, a powerful mechanism for organizing and controlling how callbacks within a node are executed. This enhances gives you a more precise way to control how tasks run within a single node.

##### Default vs. Custom Groups:
- Default Group: All subscriptions and timers created without specifying a callback group belong to the default group provided by ROS 2.
- Custom Groups: Define custom callback groups to manage specific sets of callbacks based on their execution requirements.

##### Types of Callback Groups:
- Mutually Exclusive: Callbacks within this group cannot be executed simultaneously. This is ideal for tasks that require exclusive access to shared resources or sequential execution.
- Reentrant: Callbacks within this group can be executed in parallel. This is suitable for independent tasks that don't conflict with each other.
Benefits of Callback Groups:

##### Advantages :
- Improved Control: Callback groups allow you to define how callbacks are scheduled within a Multi-Threaded Executor. This enables prioritization of critical tasks and avoids potential race conditions.
- Modular Design: Organize your node's functionality into logical groups, improving code readability and maintainability.
Advanced Concept: Executor and Callback Group Distribution

ROS 2 allows assigning different callback groups to separate Executors. This advanced technique, along with operating system scheduler configuration, enables further prioritization of specific tasks within your node.

By leveraging callback groups effectively, you can achieve more précise control over task execution within your ROS 2 nodes. This leads to improved performance, predictability, and maintainability of your robot software, especially in complex applications with diverse task requirements.

#### Scheduling Semantics

The ROS Executor plays a critical role in managing the execution of callbacks triggered by incoming messages, events, and service requests. Here's a breakdown of the scheduling logic employed by the Executor, along with insights from the provided flowchart:

##### Prioritization Based on Processing Time
The Executor prioritizes efficiency by processing callbacks with execution times shorter than the message/event arrival period in a First-In-First-Out (FIFO) manner. This ensures timely processing for tasks that don't overload the system, as reflected in the flowchart's initial processing of "Take message" and "Execute callback."

##### Queued Processing for Long-Running Callbacks
If a callback's processing time exceeds the message/event arrival period, the corresponding message or event gets queued in the lower layers of the communication stack. This prevents a single long-running callback from blocking the processing of other messages, as depicted by the message entering the "wait in middleware" stage in the flowchart.

##### Limited Queue Visibility for Scheduling
The wait set mechanism, responsible for informing the Executor about pending messages/events, provides limited information. It simply indicates the presence or absence of messages for a specific topic, not the details of the queue itself.
Due to this limited visibility, the Executor employs a round-robin approach to process messages (including services and actions) from these queues. This ensures fairness in processing messages from various topics, even if it deviates from a strict FIFO order, as illustrated by the cyclical flow between "collect_entities" and "wait in middleware" in the flowchart.

##### The Flowchart
The provided flowchart visually represents the ROS Executor's scheduling semantics:
- Take Message: The Executor retrieves a message from the wait set.
- Execute Callback: The Executor executes the corresponding callback function associated with the message.
- Clear in wait-set: The Executor removes the processed message from the wait set.
- Timer Ready?: The flowchart checks if a timer associated with a service request or action has expired:
    - Yes: The Executor processes the service request or action.
    - No: The process loops back to step 1 to check for new messages in the wait set.
- Topic in Wait-set?: The Executor inquires whether there are messages waiting for a specific topic (limited visibility):
    - Yes: The Executor moves to a round-robin fashion to process messages from this topic's queue (represented by "collect_entities").
    - No: The process loops back to step 4 to check for timers.
- Wait in Middleware: The Executor waits for the message queue (or middleware) to signal the availability of a new message.


![Scheduling semantics diagram](https://docs.ros.org/en/rolling/_images/executors_scheduling_semantics.png)


In essence, the ROS Executor balances efficiency and order in its scheduling approach. It prioritizes processing speed for short-running callbacks but implements queuing and round-robin processing for long-running tasks. This approach strives to maintain responsiveness while preventing message backlogs and ensuring fair treatment for messages from different topics.


### Interaction with OS Scheduler

However, despite executors existence, the OS scheduler (Linux Kernel scheduler) still play a role in task planning. 


#### Mastering CPU Cores

The Linux kernel scheduler manage a crucial task, the CPU core allocation.  He manages it by thread Placement. When ROS 2 Executors create threads for parallel processing within a node, the OS scheduler takes center stage. It determines which CPU core each thread will reside on. This allocation strategy significantly impacts overall system performance. While ROS 2 Executors don't directly control CPU cores, they influence this placement indirectly through the choice of Executor type (multi-threaded vs. single-threaded) and by potentially setting thread priorities


#### Even beyond CPU Cores, the OS Scheduler has competencies

While CPU core allocation is a core function of the OS scheduler, its role in a ROS 2 environment extends beyond that. Here's a breakdown of the OS scheduler's key functionalities:
- Overall Task Scheduling: The OS scheduler acts as the central component responsible for selecting which thread (created by ROS 2 Executors or other applications) gets a slice of CPU time to execute its instructions. It relies on sophisticated scheduling algorithms to ensure fair allocation of CPU time and prioritize critical tasks for optimal system performance.
- Context Switching: When the OS scheduler switches between threads, it performs a context switch. This involves efficiently saving the state (registers, memory pointers) of the currently running thread and restoring the state of the newly selected thread. Frequent context switching can introduce overhead, impacting performance. However, the OS scheduler strives to maintain a balance between fairness and efficiency.
- Preemption for Real-Time Tasks: In ROS 2, some tasks might have strict deadlines that must be met. The OS scheduler can preempt (interrupt) a lower-priority thread if a higher-priority task with deadlines needs immediate execution. This ensures timely completion of critical tasks within the robot system.
- I/O Management: Many ROS 2 tasks involve interacting with devices like network interfaces. The OS scheduler oversees these I/O operations, ensuring efficient resource utilization and preventing conflicts between ROS 2 nodes and other applications running on the system.
- Memory Management: While ROS 2 manages its own memory allocation within nodes, the OS scheduler plays a crucial role in overall system memory management. It ensures sufficient memory is available for all running threads and processes. Additionally, the OS scheduler handles swapping data between RAM and storage when necessary.


#### Cooperation Between Schedulers

ROS 2 Executors and the OS scheduler operate at different levels but cooperate to manage task execution on a multi-core system. Here's a breakdown of their interaction:
- Executor's Role:
    - Manages task scheduling within a node.
    - Decides when and how to execute tasks (callbacks) based on priorities and dependencies within the node.
    - Creates multiple threads if parallel processing is required.
- OS Scheduler's Role:
    - Allocates CPU cores to threads submitted by Executors (and other applications).
    - Determines when each thread gets a slice of CPU time to execute its instructions.
    - Manages context switching, preemption, I/O operations, and memory allocation.
- Communication: While they operate independently, communication exists between Executors and the OS scheduler. Executors submit threads for execution, and the OS scheduler allocates CPU cores based on its algorithms and system priorities. This collaborative effort ensures efficient task execution within the ROS 2 framework.


## ROS and Determinism

Determinism is crucial for specific robotics applications. Real-time systems operate within strict deadlines, ensuring timely responses to external stimuli or sensor data. These systems are essential in applications where even slight delays can have severe consequences. Examples include:
- Autonomous vehicles: Precise control over steering, braking, and acceleration is vital for safe navigation. Delays can lead to collisions or loss of control.
- Industrial robots: Reliable and predictable execution of movements is necessary for accurate task completion and to avoid safety hazards for human workers nearby.
- High-frequency trading systems: Split-second decisions based on real-time market data are crucial for success. Delays can lead to missed opportunities or incorrect trades.


### Determinism challenges in ROS2

ROS 2, while a powerful framework for robot software development, was not originally designed specifically for real-time applications. Here's how standard ROS 2 components can hinder determinism:
- Non-deterministic schedulers (rclcpp/rclpy): These schedulers prioritize ease of use and may not guarantee the order or timing of callback execution. This can lead to unpredictable behavior under high workloads, potentially causing missed deadlines for critical tasks.
- Lack of built-in prioritization: All callbacks are treated equally, regardless of their real-time importance. This can lead to delays in critical tasks if lower-priority callbacks are processed first.
- Limited scheduling options: Standard ROS 2 schedulers offer basic scheduling functionalities and lack features like user-defined execution order, trigger conditions, and advanced multi-threading support. These limitations make it difficult to achieve the fine-grained control necessary for real-time systems.
- Underlying ROS 2 components: ROS 2 relies on various software components that may introduce non-determinism. For instance, the memory allocation mechanisms used by the Data Distribution Service (DDS) might not be suitable for real-time environments.

#### Strategies for Real-time ROS 2 Development

Several approaches can help you achieve real-time performance in ROS 2 applications:
- Optimizations for the Standard Schedulers
- micro-ROS Client Library (rclc)
- Kria Robotics Stack (KRS)


##### Optimizations for the Standard Schedulers
- Memory Locking (mlockall): This technique prevents memory pages from being swapped out to disk, minimizing page faults and ensuring consistent access times for critical data.
- Thread Priority Manipulation: Assigning higher priorities to real-time control threads allows them to preempt lower-priority tasks, improving responsiveness. However, this might require elevated permissions or a real-time kernel (e.g., RT_PREEMPT) for effective control.
- Real-time Kernel Adoption: Replacing the standard Linux kernel with a real-time kernel provides more control over thread scheduling and can significantly improve real-time performance. However, this approach might require additional configuration and porting efforts for your existing ROS 2 code.


##### micro-ROS Client Library (rclc)
Designed specifically for resource-constrained environments like micro-robots, rclc prioritizes deterministic behavior and real-time execution. It offers several features that address the limitations of standard ROS 2 schedulers:
- Deterministic Callback Execution: rclc allows users to define the order of callback execution, ensuring predictable behavior even under heavy workloads.
- Triggered Execution: Callbacks can be initiated based on specific conditions, such as the arrival of a particular message or a sensor threshold being crossed. This enables efficient utilization of processing resources by only executing callbacks when necessary.
- Callback Prioritization: Assigning priorities to callbacks allows critical tasks to be processed first, even if lower-priority callbacks are delayed slightly. This prioritization ensures that deadlines for real-time tasks are met.
- Planned LET (Logical Execution Time) semantics (under development): LET semantics will facilitate data synchronization for periodic tasks in embedded systems. This is crucial for ensuring data consistency across callbacks that execute at fixed intervals.
- Multi-threading support (under development): rclc is under active development, and multi-threading support is being implemented. This will enable parallel processing of callbacks, further enhancing performance for real-time applications.


##### Kria Robotics Stack (KRS)
KRS is a comprehensive set of tools and techniques specifically designed to enable real-time ROS 2 applications on Central Processing Units (CPUs). It addresses various sources of indeterminism across hardware and software layers:
- Network Determinism: KRS utilizes Time-Sensitive Networking (TSN) to ensure deterministic communication over Ethernet. It can also bridge communication between different network buses for robust data exchange
- Operating System Determinism: KRS offers options like a fully preemptible Linux kernel (PREEMPT_RT) and a Xilinx-specific kernel with optimized drivers to minimize scheduling delays and improve real-time performance.
- Memory Management: KRS might employ techniques like real-time memory allocators to minimize the risk of unpredictable memory allocation delays that could impact deadlines.
- Cache Management: KRS might utilize cache coloring at the hypervisor level to isolate real-time applications from other processes and minimize cache interference, ensuring predictable access times for critical data.
- ROS 2 Middleware Integration: KRS aims to improve the real-time performance of ROS 2 middleware components by leveraging the underlying deterministic features offered by KRS.


##### Choosing the Right Approach
The best approach for achieving real-time performance in ROS 2 depends on your specific application's needs and resource constraints. 

For general ROS 2 applications where real-time guarantees are not critical:
- Standard ROS 2 schedulers (rclcpp/rclpy) might be sufficient due to their ease of use and compatibility with the broader ROS 2 ecosystem. However, consider basic optimizations like memory locking or exploring ROS 2 profiling tools to identify potential bottlenecks.

For micro-ROS environments demanding strict determinism and real-time execution:
- micro-ROS Client Library (rclc) is a compelling option due to its focus on deterministic behavior and features like user-defined callback order, triggered execution, and prioritization.

For real-time ROS 2 applications on CPUs with complex hardware interactions:
- Kria Robotics Stack (KRS) offers a comprehensive suite of tools and techniques to address determinism challenges across various hardware and software layers. However, using KRS might require additional development effort compared to other approaches.

Additional Considerations:
- Learning Curve: Moving from standard ROS 2 schedulers to rclc or KRS might involve a steeper learning curve due to the additional configuration and control options offered by these solutions.
- Development Status: While rclc and KRS are actively developed, some features like multi-threading support in rclc or specific hardware integrations in KRS might still be under development.


## Sources

ROS scheduler:
- https://www.oreilly.com/library/view/understanding-the-linux/0596002130/ch11s02.html#:~:text=The%20Linux%20scheduling%20algorithm%20works,have%20different%20time%20quantum%20durations.
- https://docs.kernel.org/scheduler/sched-design-CFS.html#:~:text=CFS%20picks%20the%20%22leftmost%22%20task,a%20deterministic%20amount%20of%20time.
- https://www.ibm.com/docs/en/linux-on-systems?topic=management-linux-scheduling
- 

ROS2 scheduler:
- https://wiki.ros.org/ROS/Tutorials/UnderstandingNodes
- https://docs.ros.org/en/rolling/Concepts/Intermediate/About-Executors.html
- https://micro.ros.org/docs/tutorials/programming_rcl_rclc/executor/
- https://nicolovaligi.com/articles/concurrency-and-parallelism-in-ros1-and-ros2-linux-kernel-tools/

ROS and determinism:
- https://micro.ros.org/docs/concepts/client_library/execution_management/#real-time-embedded-applications
- https://www.freertos.org/2020/09/micro-ros-on-freertos.html
- https://docs.ros.org/en/foxy/Tutorials/Demos/Real-Time-Programming.html
- https://roscon.ros.org/2015/presentations/RealtimeROS2.pdf
- https://github.com/ros-realtime
- https://design.ros2.org/articles/realtime_background.html
- https://xilinx.github.io/KRS/sphinx/build/html/docs/features/realtime_ros2.html
- https://hackmd.io/@nturacing/BkCnNlOno?utm_source=preview-mode&utm_medium=re