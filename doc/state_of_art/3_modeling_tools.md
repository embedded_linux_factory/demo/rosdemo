# Modeling and simulation tools

ROS provides a toolbox of tools to create a digital copy of a robot,  often called a "digital twin" in factories.  This digital twin is like a practice robot for your computer, allowing you to test things out before building a real one.

The main tool for describing your robot's body is called URDF.  Think of URDF like a detailed instruction sheet that explains how your robot is put together, like a plan for the robot's skeleton.  To make this digital twin even more useful, we can use a simulator like Gazebo.  Gazebo brings your URDF model to life in a virtual world, letting you see how your robot would move.  This virtual world is like a safe testing ground where you can try different ideas and fix any problems before building a real robot.


## Core Modeling Tools, the foundation

The core of building robots with ROS is a collection of tools used to create digital models of robots. These models act like a plan for everything that comes next, from testing the robot's movements in a virtual world to figuring out how to control it. This section will explain the essential tools that are like the foundation for creating robots in ROS.

### Robot Description Tools

#### URDF (Unified Robot Description Format)

##### The Building Blocks of Your Robot
URDF (Unified Robot Description Format) is an industry standard that acts like a detailed instruction manual for your robot's body. It describes the robot's skeletal framework, focusing on joints and how they connect.

##### What's in a URDF file?
- Joint Types: Different joint types are specified, like revolute (rotational) or prismatic (linear). URDF also handles continuous joints.
- Link Definitions: Individual robot segments (links) are defined, including their names, visuals (for simulation), and inertia.
- Joint Connections: The magic! URDF meticulously describes how links connect through joint origins and attachments, building the robot's structure step-by-step.

##### Benefits of URDF
- Standardized format for easy sharing between tools and research groups.
- Clear and concise way to describe complex robots.
- Foundation for robot simulation and control.

##### Example: Simple Robotic Arm
Imagine a robotic arm with a shoulder, elbow, and wrist joint. A URDF file would define these joints (including rotation limits) and the properties of the upper arm, forearm, and gripper. It would then detail how these links connect, building the complete arm structure.

URDF is the digital plan for your robot, providing a clear and standard way to describe its skeletal framework and joints. This is essential for simulations, control, and bringing your robot to life.

![robot model example](https://raw.githubusercontent.com/ros/urdf_tutorial/ros2/images/visual.png)

#### xacro

##### Making URDF Less Tedious
URDF is great for describing robots, but complex models with many identical components can become repetitive to write, we can use tools like xacro to fix it. Xacro is a special coding language built on XML that simplifies URDF creation, especially for repetitive elements.

##### Benefits of xacro:
- Reduces Complexity: Complex URDF files become more manageable and easier to maintain, especially for robots with repetitive components.
- Saves Time: Reusing macros and leveraging loops significantly reduces coding time.
- Increases Efficiency: Makes robot model creation more streamlined and less prone to errors.

#### SRDF

While URDF focuses on a robot's body (joints and links), SRDF describes its "brain" – the sensors and actuators that make it smart.  Think of it as an extra instruction sheet for your robot.

##### What's in an SRDF file?
- Sensors: Details about the robot's eyes and ears (cameras, LiDAR), where they are mounted (attached to which link), and what kind of information they pick up (color image, 3D point cloud).
- Actuators: Information about the robot's muscles (motors), what joints they control, and how strong they are (maximum force).

##### Benefits
- Smarter Robot Simulations: Imagine a robot with a camera. SRDF lets simulators know the camera exists and what it "sees," making simulations more realistic.
- Better Robot Planning: Planning how a robot does its job considers its sensors and actuators. SRDF tells the planner what information the robot can sense and how strong it is, allowing for better task plans.

#### SDF (Robot Simulation Description Format)

While URDF and SRDF describe a robot's physical structure and capabilities, SDF (Robot Simulation Description Format) focuses on making those capabilities come alive in simulation.  It is as a settings file specifically for robot sensors within Gazebo, the popular robot simulator. So a SDF file give robot sensors specification,

##### Benefits
- Realistic Robot Behavior: By incorporating sensor limitations and noise, SDF ensures simulated robots behave similarly to their real-world counterparts. So it reduces the number of uncertainties robots face in real-world scenarios.
- Improved Algorithm Development: Realistic simulations with SDF allow researchers to test and refine robot algorithms in a safe virtual environment before deploying them on real robots.

### Simulation tools

#### Gazebo (Open-source)

##### Key Features
- Rich Physics Engine: Accurately simulates robot dynamics, including factors like inertia, friction, and center of gravity.
- Sensor Simulation: Provides realistic sensor data (lidar, camera, etc.) for robots within the simulation environment.
- World Building Tools: Allows you to create complex virtual environments with obstacles, terrain variations, and other objects for robot interaction.
- Plugin Support: Extensible architecture allows for integration with various plugins for specific needs (e.g., custom robot models, advanced sensor simulation).

##### How it Works
Gazebo stands as the workhorse simulator within ROS. It excels at simulating a wide range of robots, from wheeled robots navigating warehouses to legged robots traversing rough terrain. Imagine developing a robot for search and rescue operations.  Using Gazebo, you can:
- Model the Robot: Define the robot's physical characteristics (links, joints, sensors) within Gazebo.
- Create a Virtual Environment: Design a virtual environment replicating a potential search and rescue scenario, including buildings, rubble piles, and uneven terrain.
- Simulate Robot Behavior: Develop control algorithms for the robot to navigate the environment autonomously. Gazebo simulates the robot's movements, sensor data, and interactions with objects within the virtual environment.  This allows you to test and refine your algorithms before deploying the robot in a real-world search and rescue operation.

##### Benefits:
- Versatile and powerful simulation platform for a wide range of robots.
- Realistic physics engine and sensor simulation for comprehensive testing.
- Open-source and constantly evolving with contributions from the ROS community.
- Extensive plugin support for customization and advanced functionalities.

#### rqt_graph

##### Key Features
- Interactive GUI: Provides a user-friendly interface to explore communication between ROS nodes.
- Live Updates: Dynamically displays data flow (topics) as your ROS application runs.
- Debugging Aid
- ROS Integration: Seamlessly integrates with ROS, automatically discovering nodes and topics.

##### Benefits
- Clearer Development: Simplifies development and debugging by visualizing communication flow.
- Deeper System Understanding: Helps developers grasp complex ROS applications with numerous nodes and data topics.
- Effective Debugging: Aids in pinpointing communication bottlenecks and troubleshooting data exchange issues.


### CAD (Computer-Aided Design) Integration Tools

#### URDF Exporters for CAD Software

Many popular Computer-Aided Design (CAD) programs, like Solidworks and Fusion 360, now recognize the importance of robotics.  They offer built-in functionalities or plugins called URDF exporters.  These exporters act as intelligent translators, converting your robot design from your CAD software directly into a URDF file usable within ROS.

##### Benefits
- Faster Development: Eliminate the need for hand-coding complex robot descriptions in URDF format. The URDF exporter automates the translation process, saving time and effort.
- Reduced Errors: Manual coding can introduce errors. URDF exporters ensure an accurate conversion of your robot design into the ROS-compatible format, minimizing the risk of errors that could lead to problems in simulation or deployment.
- Smoother Workflow: URDF exporters bridge the gap between the design and simulation phases. You can seamlessly transition from designing your robot in CAD software to simulating it within ROS, streamlining the overall development process.

##### Examples
- Solidworks: Utilizes the "ROS Industrial URDF Export" add-on, a powerful tool specifically designed for this purpose.
- Fusion 360: Offers a built-in "URDF Export" function, providing a convenient way to export your robot model directly from the software.


### Robot State Publisher

#### RVIZ

Imagine RVIZ as a joystick for simulated robot:
- Move the Robot: Use sliders and buttons to bend the robot's joints, like a virtual joystick. See how it moves in real-time!
- Test and Debug: Try different motions to test if your robot can reach things or if its design works as planned.

RVIZ can also talk to other programs in the simulation:
- Share Robot Info: RVIZ can tell other programs (like control algorithms) where the robot's joints are and what its sensors "see" (if it has any).
- Better Control Algorithms: Control algorithms can use this information to make decisions about how to move the robot.

##### Benefits:
- Easier to test robots without needing a real one.
- Find problems with the robot's design or how it moves.
- See how the robot would react in different situations.


![RVIZ using example](https://docs.ros.org/en/kinetic/api/moveit_tutorials/html/_images/rviz_plugin_head.png)

#### X3D (Extensible 3D)

Unlike URDF's focus on robots, X3D offers a more general-purpose language for representing various 3D elements:
- Geometry: X3D can describe basic shapes like cubes, spheres, and even complex geometries used in robot models.
- Textures: Want your robot arm to look like polished metal? X3D allows you to incorporate textures for a more realistic visual representation.
- Animations: X3D even supports animations! Imagine simulating a robot arm going through its motions using X3D.

##### Benefits of X3D for Robot Descriptions:
- Integration from External Sources: If you have a 3D robot model created in a non-ROS program that supports X3D export, you could potentially use X3D as a bridge to bring that model into ROS.
- Richer Visuals: X3D's support for textures and animations could allow for more visually appealing robot simulations within ROS (although functionalities might be limited compared to dedicated visualization tools like RVIZ).

#### COLLADA (COLLAborative Design Activity)

Think of COLLADA as a common language for 3D data exchange. Similar to X3D, it offers a generic way to represent 3D models, acting as a bridge between different design software programs.

##### Benefits of COLLADA for Robot Descriptions
- External Model Import: Similar to X3D, COLLADA could theoretically be used to import 3D robot models from other design tools into ROS. Imagine designing a robot arm in a program outside of ROS and wanting to use it within ROS simulations. COLLADA could potentially facilitate that transfer.

##### Challenges of COLLADA in ROS
- Focus on Generality: Like X3D, COLLADA's strength lies in its broad applicability across various 3D data. This generality can lead to compatibility issues when integrating robot models specifically designed for other programs into ROS, facing similar challenges as X3D regarding missing kinematic details or requiring additional processing.
- Limited ROS Support: The ROS community prioritizes URDF development. Support for COLLADA within ROS might be limited, potentially leading to troubleshooting difficulties.


## Advanced Modeling Tools

As your robotics endeavors venture into intricate industrial applications, the core ROS modeling tools might not always suffice. Here, a diverse array of advanced modeling tools offering functionalities specifically tailored for robots with complex dynamics, demanding control requirements, or the need for high-fidelity simulations.


### Specialized Robot Modeling Frameworks

Imagine a scenario where you're designing a high-speed robotic arm for a demanding industrial task or a humanoid robot capable of complex manipulation.  Here's where Specialized Robot Modeling Frameworks (RMFs) come in.  These frameworks cater to robots with intricate dynamics, providing high-fidelity simulation capabilities that go beyond the core ROS modeling tools.  They offer a robust foundation for designing and testing control strategies for these complex robots within a virtual environment.  Here are some prominent examples of RMFs:

#### Drake (Open-source)

##### Key Features
- High-fidelity simulation: Accurately model complex robot dynamics for precise control design.
- Open-source: Freely available and customizable for specific needs.

##### Example 
Simulating a high-speed robotic arm in a factory environment to design a controller that ensures accurate and stable operation at high speeds.

##### Benefits
- Enables design and testing of control strategies for complex robots with intricate dynamics.
- Open-source nature allows for customization and integration with other tools.


### Additional Advanced Tools to expand your toolkit

While RMFs provide a powerful foundation for complex robot simulations, here are some additional specialized tools that can address specific needs within industrial robotics:

#### MuJoCo

##### Key features
- Multi-body dynamics simulation: Accurately model complex robots with many interconnected parts.
- Control design and optimization: Ideal for designing and refining controllers for complex robots.

##### How it works 
MuJoCo utilizes a powerful physics engine specifically tailored for simulating the dynamics of complex robotic systems.  This allows for highly realistic simulations that accurately capture the robot's behavior under various conditions.

##### Example
Imagine simulating a bipedal robot for walking or running tasks. MuJoCo can be used to design and optimize control algorithms that ensure stable and efficient locomotion gaits.  Similarly, you could simulate a multi-armed robot performing delicate manipulation tasks, allowing you to refine the robot's controllers for precise movements.

##### Benefits
- Realistic simulation environment for complex robots.
- Ideal for control design, optimization, and robot motion planning.

Considerations: MuJoCo requires a commercial license for commercial use.

#### OpenSCENARIO and others ROS scenario simulation

##### Key Features
- Standardized scenario description: Defines driving scenarios for autonomous vehicles in a common format.
- Open-source: Freely available for creating and customizing scenarios.

##### How it Works
OpenSCENARIO provides a standardized language for describing the elements of a driving scenario for autonomous vehicles.  This includes elements like traffic lights, road signs, lane markings, and the behavior of other vehicles.  By defining these elements within an OpenSCENARIO file, you can create complex virtual environments for testing the control algorithms of autonomous vehicles within ROS.

##### Example
Imagine developing self-driving car algorithms.  Using OpenSCENARIO, you could create virtual test tracks with various obstacles, traffic patterns, and weather conditions.  This allows you to rigorously test the ability of your autonomous vehicle to navigate these diverse scenarios in a safe and controlled simulation environment.

##### Benefits
- Standardized format simplifies creation and sharing of driving scenarios.
- Enables testing of autonomous vehicle algorithms in diverse virtual environments.

Considerations: OpenSCENARIO focuses specifically on creating scenarios for autonomous vehicles.

#### DART

##### Key Features
- High-performance multi-body dynamics simulation: Handles complex robots with real-time performance.
- Open-source: Freely available for research and development purposes.

##### How it Works
DART leverages advanced algorithms to perform physics simulations of complex robots with exceptional speed and accuracy.  This makes it ideal for situations where real-time performance or extremely high-fidelity simulations are crucial.

##### Example
Imagine simulating a swarm of robots collaborating on a task in a warehouse or a high-speed robotic manipulator performing intricate assembly tasks in a factory.  DART's capabilities ensure the simulations accurately reflect the complex dynamics of these scenarios, allowing you to design and test control algorithms effectively.

##### Benefits
- Real-time performance for fast-paced simulations.
- High-fidelity dynamics for complex robots.

Considerations: DART's focus on high-performance simulations might come with a steeper learning curve compared to some other tools.

By incorporating these advanced tools alongside core ROS modeling tools and Specialized Robot Modeling Frameworks, you can create a powerful and versatile modeling environment for tackling even the most intricate industrial robotics challenges.


## Specialized Modeling Tools (Industry-Specific Applications):

The versatility of ROS extends across various industries. 

### All fields

#### ROS-Industrial Consortium

This collaborative effort acts as a hub for ROS in various industries. While we've focused on manufacturing and agriculture, the ROS-Industrial Consortium offers resources and tools for numerous other sectors. Their website is a valuable starting point for exploring industry-specific packages that might provide specialized modeling tools for your specific application. Imagine developing a robot for construction, logistics, or healthcare; the ROS-Industrial Consortium might have the resources to streamline your modeling process.

For further information : https://rosindustrial.org/

Now let's delve into some examples of specialized modeling tools designed to streamline development within specific application domains:

### Manufacturing:

Industrial robots play a crucial role in modern manufacturing, performing tasks ranging from welding and painting to assembly and material handling. To ensure these robots function efficiently and safely, powerful simulation and programming tools are essential. Here, we'll explore some key software options specifically designed to streamline the development process for industrial robotics within ROS:

#### ABB Robot Studio

##### Key Features:
- Integrated ROS Environment: Familiar ROS interface for robot modeling, simulation, and programming.
- Offline Programming: Develop and test robot programs without needing the physical robot.
- Virtual Manufacturing Simulation: Simulate robot tasks like welding or painting within a virtual factory environment.

##### How it Works
Robot Studio provides a comprehensive suite specifically tailored for ABB robots.  It seamlessly integrates with ROS, allowing you to leverage the power of ROS for robot modeling, simulation, and programming within a familiar interface.  A key feature is offline programming, which enables you to develop and test robot programs in a virtual environment without needing the physical robot.  This saves time and resources, allowing for efficient program development and refinement before deployment on the factory floor.  Additionally, Robot Studio offers functionalities for simulating robot tasks within a virtual factory setting.  Imagine designing a robotic welding application for an automotive assembly line.  Robot Studio allows you to import CAD models of the car parts and simulate the welding process virtually, ensuring the robot's movements are accurate and efficient.

##### Benefits
- Streamlined development process for ABB robots within ROS.
- Offline programming capability for efficient program creation and testing.
- Virtual manufacturing simulation for validating robot performance in a factory setting.


### Agriculture:

#### AgROS

##### Key Feature
- Customizable Field Creation: AgROS allows users to import real-world field data to create a virtual replica. This includes details like terrain variations, obstacles (trees, fences), and specific crop types. This level of customization ensures highly realistic testing scenarios that mimic the complexities of a real farm.
- Simulated Robot Movement: Test robot navigation algorithms, path planning strategies, and sensor data collection capabilities within the virtual environment. AgROS simulates robot movement, allowing developers to identify potential issues and optimize robot behavior before deploying them in real fields.
- Decision Support Tool: Analyze the data generated during simulations to gain valuable insights. This data can be used to optimize robot performance and make informed decisions about real-world agricultural practices, such as resource allocation and robot deployment strategies.

##### How It Works
- Built on ROS (Robot Operating System): AgROS leverages the power of ROS, a popular framework for robot software development. This provides a robust foundation for simulating agricultural tasks.
- Virtual Farm Environment Creation: Farmers can import data from their actual fields or create custom virtual environments. This includes details like terrain, obstacles, and crop types, ensuring realistic testing scenarios.
- Data Analysis and Optimization: AgROS allows for the simulation of robot movement and sensor data collection. By analyzing this data, developers and farmers can identify areas for improvement and optimize robot performance before real-world deployment.

##### Benefits
- More Efficient Robot Development: Testing and refining robots in a virtual environment reduces the need for expensive physical prototypes and field trials.
- Improved Robot Performance in Fields: AgROS allows for iterative testing and optimization, leading to more robust and efficient robots when deployed in real fields.
- Data-Driven Farm Management: The data generated by AgROS simulations provides valuable insights for optimizing agricultural practices and robot deployment strategies, leading to increased farm productivity and sustainability.


### Logistics:

The world of logistics relies heavily on automation to ensure efficient movement of goods. ROS plays a vital role in this domain, and here are some key tools that empower you to enhance your modeling capabilities for logistics applications:

#### MoveIt! (Open-source)

##### Key Features
- Motion Planning: Generates collision-free paths for robot arms, ensuring safe and efficient movement within the warehouse environment.
- Inverse Kinematics: Computes joint configurations for the robot arm to reach desired poses (positions and orientations) for grasping and placing objects.
- Simulation Integration: Integrates seamlessly with ROS-Industrial Logistics packages and other robot simulators to enable realistic simulation of robot arm movements.
- Open-source and Extensible: Freely available and allows customization for specific robot arm configurations and gripper types.

##### How it Works
MoveIt! plays a crucial role in simulating manipulation tasks for logistics robots, particularly those involving robotic arms.  Imagine simulating a robotic arm equipped with a gripper for picking boxes from shelves within a warehouse.  Here's how MoveIt! contributes:
- Gripper Definition : MoveIt! allows you to define the robot arm's gripper as an end-effector within the ROS simulation environment.
- Pose Specification: You can specify the desired poses (position and orientation) for the gripper to grasp and place boxes. 
- Collision-free Path Planning: MoveIt! then computes a collision-free path for the robot arm to reach the desired pose. This ensures the robot arm avoids obstacles and other objects in the environment while performing picking and placing tasks.

##### Benefits
- Simulate complex robot arm movements for picking, placing, and other manipulation tasks within the virtual warehouse.
- Ensures collision-free trajectories for safe robot operation, minimizing the risk

![Task constructor - on the website](https://moveit.ros.org/assets/images/screens/moveit_task_constructor.png)

### Construction:

#### ROS-Industrial Consortium - Building 

(ref. "All fields")

### Healthcare

#### ROS-Industrial Consortium - Healthcare 

(ref. "All fields")


### Aerospace

The final frontier of space exploration is embracing the power of ROS for developing and testing robotic systems. Here, we explore some key tools that empower you to enhance your modeling capabilities for aerospace applications within ROS:

#### ROS-Industrial Consortium - Aerospace (Open-source)

(ref. "All fields")

#### FlightGear (Open-source)

##### Key Features:
- Open-source
- ROS Integration: Connects with ROS, allowing control and simulation of ROS-based robot models within the FlightGear environment.
- Realistic 3D Environments: Offers virtual representations of airports, terrains, and weather conditions.

##### How it Works
FlightGear is a popular open-source flight simulator that can be interfaced with ROS.  Imagine simulating a drone for aerial inspection tasks within a virtual airport environment.  By integrating a ROS-controlled drone model with FlightGear, you can leverage the strengths of both platforms.  FlightGear provides the realistic 3D environment, including virtual buildings, runways, and even simulated weather conditions.  ROS, on the other hand, allows you to control the drone's flight path, sensor data, and mission logic within the simulation.  This combined approach enables comprehensive testing and refinement of your drone's functionalities before deploying it for real-world aerial missions.

##### Benefits:
- Enables simulation of aerial robots (drones, UAVs) within a realistic virtual airspace environment.
- Provides a safe and controlled space to test flight dynamics, sensor data processing, and mission functionalities.
- Freely available and open-source, making it accessible for various research and development projects.

![FlightGear Simulation · PX4 Developer Guide](https://dev.px4.io/v1.11_noredirect/assets/simulation/flightgear/flightgearUI.jpg)

#### AirSim (Open-source)

##### Key Features
- Drone and UAV Support: Specifically designed for simulating the flight dynamics and sensor data of drones and other unmanned aerial vehicles (UAVs).
- Integration with Unreal Engine: Leverages the Unreal Engine's rendering capabilities for high-fidelity visual simulations of outdoor environments.
- ROS Compatibility: Seamless integration with ROS for control and data exchange between your robot code and the AirSim simulation environment.

##### How it Works
AirSim caters to the specific needs of aerial vehicle development within ROS. Imagine developing an autonomous drone for package delivery.  AirSim allows you to:
- Model the Drone: Define the drone's physical properties (motors, propellers, sensors) within AirSim.
- Create a Virtual Airspace: Design a virtual environment replicating an urban area with buildings, obstacles, and potential landing zones for your delivery drone.
- Simulate Drone Flight: Develop control algorithms for the drone to navigate autonomously within the virtual airspace. AirSim simulates the drone's flight dynamics, sensor data (cameras, GPS), and interactions with the environment. This allows you to test and refine your algorithms for safe and efficient autonomous flight before deploying the drone for real-world deliveries.

##### Benefits:
- Ideal for developing and testing control algorithms for drones and other UAVs within ROS.
- Integration with Unreal Engine provides visually stunning and realistic outdoor environments for simulation.
- Open-source and actively maintained by a growing developer community.

![Microsoft AirSim v1.2 and Unreal 4.18 connected to ROS Kinetic and RVIZ](https://i.ytimg.com/vi/Ubqx9WifekQ/maxresdefault.jpg)

## Sources

Core Modeling Tools:

Robot Description Tools:
- https://docs.ros.org/en/rolling/Tutorials/Intermediate/URDF/Building-a-Visual-Robot-Model-with-URDF-from-Scratch.html#finishing-the-model
- http://wiki.ros.org/xacro
- https://docs.ros.org/en/foxy/Tutorials/Intermediate/URDF/Using-Xacro-to-Clean-Up-a-URDF-File.html
- http://wiki.ros.org/srdf
- https://tesseract-docs.readthedocs.io/en/latest/_source/intro/srdf_doc.html
- http://sdformat.org/

CAD:
- https://www.quora.com/What-is-the-best-computer-aided-design-CAD-software-for-robotic-design
- https://www.3ds.com/fr/products/solidworks
- http://wiki.ros.org/sw_urdf_exporter/Tutorials/Export%20an%20Assembly
- https://www.f3df.com/fusion-360-le-nouveau-logiciel-de-la-cao-3d/

Robot state publisher:
- http://wiki.ros.org/rviz
- https://automaticaddison.com/what-is-the-difference-between-rviz-and-gazebo/
- https://fr.wikipedia.org/wiki/Extensible_3D
- https://www.web3d.org/x3d/what-x3d
- https://www.d-booker.fr/content/128-obj-x3d-collada-quel-format-3d-pour-quelle-finalite-
- https://en.wikipedia.org/wiki/COLLADA
- https://docs.blender.org/manual/fr/dev/files/import_export/collada.html

Simulation tools:
- https://learn.ros4.pro/fr/simulation/gazebo/
- https://gazebosim.org/home
- https://github.com/gazebosim
- https://docs.ros.org/en/humble/Tutorials/Advanced/Simulators/Gazebo/Gazebo.html
- http://wiki.ros.org/rqt_graph
- https://roboticsbackend.com/rqt-graph-visualize-and-debug-your-ros-graph/
- https://docs.ros.org/en/rolling/Tutorials/Beginner-CLI-Tools/Understanding-ROS2-Topics/Understanding-ROS2-Topics.html


Advanced Modeling Tools:

Specialized Robot Modeling Frameworks:
- https://github.com/sloretz/drake_ros2_demos
- https://medium.com/toyotaresearch/drake-model-based-design-in-the-age-of-robotics-and-machine-learning-59938c985515
- https://github.com/RobotLocomotion/drake-ros
- https://drake.mit.edu/#core

Additional Advanced Tools to expand your toolkit:
- https://github.com/shadow-robot/mujoco_ros_pkgs
- https://mujoco.org/
- https://github.com/arminstr/ros_scenario_simulation
- https://carla.readthedocs.io/projects/ros-bridge/en/latest/carla_ros_scenario_runner/
- https://dartsim.github.io/
- https://pub.dev/packages/dartros
- https://github.com/Sashiri/ros_nodes


Specialized Modeling Tools (Industry-Specific Applications):

All fields:
- https://rosindustrial.org/

Manufacturing:
- http://wiki.ros.org/abb_driver/Tutorials/RobotStudio
- https://new.abb.com/products/robotics/robotstudio
- https://www.directindustry.fr/prod/abb-robotics/product-30265-1882653.html

Agriculture:
- https://www.mdpi.com/2073-4395/9/7/403

Logistics:
- https://moveit.ros.org/
- https://ros-planning.github.io/moveit_tutorials/
- https://www.theconstruct.ai/ros-movelt/
- https://moveit.ros.org/documentation/concepts/
- https://moveit.picknik.ai/main/index.html

Aerospace:
- https://github.com/rafael1193/skyscannner_integration
- https://dev.px4.io/v1.11_noredirect/en/simulation/flightgear.html
- https://github.com/topics/flightgear?l=python
- https://wiki.flightgear.org/FlightGear
- https://microsoft.github.io/AirSim/airsim_tutorial_pkgs/
- https://airsim-fork.readthedocs.io/en/latest/ros.html
- https://github.com/microsoft/AirSim/issues/2850
- https://microsoft.github.io/AirSim/
- https://www.helicomicro.com/2022/07/19/microsoft-airsim-est-abandonne-place-au-project-airsim/
