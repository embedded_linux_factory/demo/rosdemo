# ROSdemo


Welcome to our Yocto-based project for building custom images featuring the Robot Operating System (ROS). Empower your embedded systems with ROS capabilities by utilizing our Yocto recipes and configurations to create customized images for your robotic applications.

## Import a ROS package in your image and build it

Once you created your ROS package, add it in the repository $TOPDIR/meta-ros-on-stm32/recipes-ros-packages/ . For Example, it was already done with the talker-listener package.

Then, add the name of your package in the file conf/local.conf :
IMAGE_INSTALL:append = " <package_name>"

To build an image with your new ROS package, you have to execute this command :
```bash
./build_image.sh
```

## Add servers to avoid having to reflash the SD card for every test (optionnal)

To gain some time you can set up servers to import datas without flashing the card at each new configurations :
-  Dropbear ssh server to import the rootfs;
- TFTP server to import the Linux Kernel image and Device Tree.


### Add Dropbrear SSH server

The dropbear package is already add in your image in your file conf/local.conf.

1. First we need to set the kernel boot arguments U-Boot will pass to the Linux kernel at boot time. For that, edit the extlinux configuration file, in the bootfs partition of the SD card and change the APPEND line to:
```bash
APPEND root=/dev/nfs rw console=ttySTM0,115200 nfsroot=192.168.0.1:/nfs,vers=3,tcp ip=192.168.0.100
```
• For the stm32mp157a-dk1 board, edit the mmc0_extlinux/extlinux.conf file.
• For the other boards, edit the corresponding file (for example mmc0_extlinux/stm32mp157d-dk1_extlinux.conf for the stm32mp157d-dk1 board).

2. Then with a network cable, connect the Ethernet port of your board to the one of your computer. If your computer already has a wired connection to the network, your instructor will provide you with a USB Ethernet adapter.
A new network interface should appear on your Linux system. Find the name of this interface by typing:
```bash
ip a
```
The network interface name is likely to be enxxx2
. If you have a pluggable Ethernet device, it’s easy to identify as it’s the one that shows up after pluging in the device.

Then, instead of configuring the host IP address from NetworkManager’s graphical interface, let’s do it through its command line interface, which is so much easier to use:
```bash
nmcli con add type ethernet ifname en... ip4 192.168.0.1/24
```

3. Futhermore, to set up the NFS server on the workstation you have to install the NFS server on the training computer and create the root NFS directory:
```bash
sudo apt install nfs-kernel-server
sudo mkdir -m 777 /nfs
```

4. Then make sure this directory is used and exported by the NFS server by adding the below line to the /etc/exports file:
```bash
/nfs *(rw,sync,no_root_squash,subtree_check)
```
5. Finally, make the NFS server use the new configuration and put the rootfs under the NFS root directory so that it is accessible by NFS clients:
```bash
sudo exportfs -r
sudo tar xpf $TOPDIR/tmp-glibc/deploy/images/stm32mp1/ros-image-core-humble-stm32mp1.tar.xz -C /nfs
```

For the next builds, you won't have to repeat the process in step 5, these commands are in the script build_image.sh .

6. The Dropbear SSH server was enabled a few steps before, and should now be running as a service on the Discovery. You can test it by accessing the board through SSH :
```bash
ssh root@192.168.0.100
```

You should see the Discovery command line!


### Add TFTP server

First, install a TFTP server (package tftpd-hpa) on your system.

Then copy the Linux kernel image and Device Tree to the TFTP server home directory (specified in /etc/default/tftpd-hpa) so that they are made available by the TFTP server.

Then, in the U-Boot shell, change the bootcmd variable to load the kernel image and the Device Tree over TFTP (replace dtb by the actual Device Tree file for your board):
```bash
setenv ipaddr 192.168.0.100
setenv serverip 192.168.0.1
setenv bootcmd 'tftp 0xc2000000 zImage; tftp 0xc4000000 dtb; bootz 0xc2000000 - 0xc4000000'
```

Still in the U-Boot shell, set the bootargs specifying the kernel command line that we previously set in extlinux.conf:
```bash
setenv bootargs root=/dev/nfs rw console=ttySTM0,115200 nfsroot=192.168.0.1:/nfs,vers=3,tcp ip=192.168.0.100
saveenv
```