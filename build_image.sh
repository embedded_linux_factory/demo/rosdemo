#!/bin/bash

# Define the path to openembedded-core
OE_CORE_PATH="openembedded-core"

# Initialize the build environment
source $OE_CORE_PATH/oe-init-build-env

# Build the ROS image
bitbake ros-image-core

# Update NFS exports
sudo exportfs -r

# Extract the built image to the NFS directory
sudo tar xpf ../tmp-glibc/deploy/images/stm32mp1/ros-image-core-humble-stm32mp1.tar.xz -C /nfs

# Copy the Linux kernel image and the the Device Tree Blob (DTB) file to the TFTP server directory
# Change the dtb file according to you board
sudo cp ../tmp-glibc/deploy/images/stm32mp1/kernel/zImage /srv/tftp
sudo cp ../tmp-glibc/deploy/images/stm32mp1/kernel/stm32mp157f-dk2.dtb /srv/tftp 
